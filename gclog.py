#!/usr/bin/env python

import re
import sys
import arrow
import json
import time

# FullGC statistics
fgc_stat = {}

# Line formats

# For now just check if it starts with iso-8601 timestamp
logEntryPattern = re.compile(
    "^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+[+-]\d{4}: "  # Timestamp
)

fullGCPattern = re.compile(
    "^(?P<ts>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+[+-]\d{4}): "  # Timestamp
    "\d+\.\d+: "                                                    # Time
    "\[Full GC\s\((?P<cause>.*)\)\s*"                               # Cause
    "(?P<before>\d+[BKMG])->(?P<after>\d+[BKMG])"                   # GC mem
    "\((?P<total>\d+[BKMG])\),\s*"                                  # Total mem
    "(?P<duration>\d+\.\d+)\ssecs"                                  # Duration
)

gcPausePattern = re.compile(
    "^(?P<ts>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+[+-]\d{4}): "  # Timestamp
    "\d+\.\d+: "                                                    # Time
    "\[GC pause\s\((?P<action>[\s\w]+)\)\s*"                        # Action
    "\((?P<collector>[\s\w]+)\).?\s*"                               # Collector
    "(\((?P<cause>.*)\))?.?\s*"                                     # Cause
    "(?P<duration>\d+\.\d+)\ssecs"                                  # Duration
)

gcEventPattern = re.compile(
    "^(?P<ts>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+[+-]\d{4}): "  # Timestamp
    "\d+\.\d+: "                                                    # Time
    "\[GC (?P<event>"                                               # Event
    "concurrent-mark-end|"
    "concurrent-root-region-scan-end|"
    "concurrent-cleanup-end|"
    "cleanup|"
    "remark).*"
    "(?P<duration>\d+\.\d+)\ssecs"                                  # Duration
)


def human_to_bytes(size):
    """Convert human readable sizes to bytes"""
    if (size[-1] == 'B'):
        size = size[:-1]
    if (size.isdigit()):
        bytes = int(size)
    else:
        bytes = size[:-1]
        unit = size[-1]
        if (bytes.isdigit()):
            bytes = int(bytes)
            if (unit == 'G'):
                bytes *= 1073741824
            elif (unit == 'M'):
                bytes *= 1048576
            elif (unit == 'K'):
                bytes *= 1024
            else:
                bytes = 0
        else:
            bytes = 0
    return bytes


def isLogLine(line):
    """See if the line looks like a log entry we need."""
    if re.match(logEntryPattern, line):
        return True
    return False


def getTS(line):
    """Parse log entry's timstamp"""
    return arrow.get(line)


def parseFullGC(line):
    """Process FullGC entry."""
    m = re.match(fullGCPattern, line)
    if not m:
        return None

    freed = human_to_bytes(m.group("before")) - \
        human_to_bytes(m.group("after"))
    freed_pcnt = round(freed / human_to_bytes(m.group("total")) * 100, 2)
    ts = getTS(m.group("ts"))

    event = {
        "eventType": "FullGC",
        "timestamp": ts.timestamp,  # UTC
        "cause": m.group("cause"),
        "before": m.group("before"),
        "after": m.group("after"),
        "total": m.group("total"),
        "freed": freed,
        "freed_pcnt": freed_pcnt,
        "duration": m.group("duration")
    }

    # Update Full GC statistics
    # It should be in a separate function, even in a separate script, but let's
    # leave it here for now

    stat_min = ts.format('YYYYMMDDHHmm')
    stat_list = fgc_stat.get(stat_min, [])

    stat_list.append(event)
    fgc_stat[stat_min] = stat_list

    return event


def parseGCPause(line):
    """Process GC pause entry."""
    m = re.match(gcPausePattern, line)
    if not m:
        return None

    ts = getTS(m.group("ts"))

    event = {
        "eventType": "GC Pause",
        "timestamp": ts.timestamp,  # UTC
        "action": m.group("action"),
        "collector": m.group("collector"),
        "cause": m.group("cause"),
        "duration": m.group("duration")
    }
    return event


def parseGCEvent(line):
    """Process GC pause entry."""
    m = re.match(gcEventPattern, line)
    if not m:
        return None

    ts = getTS(m.group("ts"))

    event = {
        "eventType": "GC Event",
        "timestamp": ts.timestamp,  # UTC
        "event": m.group("event"),
        "duration": m.group("duration")
    }
    return event


def parseLine(line):
    """Parse log line by guessing type and return event to report"""
    # Not very elegant, but it's simple enough to read and modify
    event = parseFullGC(line)
    if event is not None:
        return event

    event = parseGCPause(line)
    if event is not None:
        return event

    event = parseGCEvent(line)
    if event is not None:
        return event

    return None


def restartTomcat():
    """Restart local Tomcat server"""
    # Here we need to call AWS API to check if other Tomcat servers are
    # considered alive by ELB/ALB. If our server is not the only one in the
    # group, we may restart Tomcat.

    # Send alert
    event = {
        "eventType": "FullGC Tomcat Restart",
        "timestamp": int(time.time())
    }
    return event


def isTomcatRestartNeeded():
    """Analyze FullGC statistics and see if we need Tomcat restat"""
    needTomcatRestat = False

    # Count the number of events where less then 20% of memory was freed.
    latestMin = sorted(fgc_stat.keys(), reverse=True)[0]
    latest5Mins = [str(x)
                   for x in range(int(latestMin), int(latestMin) - 5, -1)]
    fiveMinWin = [fgc_stat.get(k, []) for k in latest5Mins]

    allFreed = [val['freed_pcnt']
                for sublist in fiveMinWin for val in sublist]
    if sum(x < 20 for x in allFreed) > 10:
        needTomcatRestat = True

    # Check if total FullGC time is over 15% of 5 minutes window
    allDuration = [val['duration']
                   for sublist in fiveMinWin for val in sublist]
    if sum(float(x) for x in allDuration) > 45:  # 15% of 5 min is 45 sec
        needTomcatRestat = True

    # Cleanup FullCG statistics
    if needTomcatRestat:
        fgc_stat.clear()
        return needTomcatRestat

    for k in list(set(fgc_stat.keys()) - set(latest5Mins)):
        fgc_stat.pop(k, [])
    return needTomcatRestat


def sendEvent(event):
    """Pretends it sends New Relic Insights event."""
    # In reality I'd use New Relic's API module here, but I don't have active
    # account there, so, as written in the task, I'll just print the JSON.
    # Emulate sending of a list of one event. Events are not grouped.
    print(json.dumps(
        [event],
        indent=2, sort_keys=True))


def main():
    while True:
        line = sys.stdin.readline()
        # Just in case there will be only cat and no tail.
        if not line:
            break
        # No need to crash. We only collect what we can.
        if not isLogLine(line):
            continue

        event = parseLine(line)
        if event is not None:
            sendEvent(event)

        if event is not None and \
           event["eventType"] == "FullGC" and \
           isTomcatRestartNeeded():
            sendEvent(restartTomcat())


if __name__ == "__main__":
    main()
