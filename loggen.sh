#!/bin/bash
# Simple script to emulate JVM GC log
# Data is random and not aligned with reality

# TODO: Add getcmd
OUTFILE="/tmp/test-gc.log"
SLEEP=1
SPLAY=5
TOTALM=64000

################################################################################
# Subs
################################################################################

# Full GC
# The template is: $DATETIME: TIME: [Full GC ($CAUSE), SOMEDATA, $DURATION secs]
FULLGC="%s: 6689.759: [Full GC (Allocation Failure)  %dM->%dM(%dM), %d.4572036 secs]\n"

writeFullGC() {
    # $1 - freed ratio
    # $2 - duration
    local before=$(($TOTALM*$(((RANDOM % 40)+60))/100))
    printf "$FULLGC" $(date +%Y-%m-%dT%H:%M:%S.666%z) $before $(($before*$1/100)) $TOTALM $2 >> "$OUTFILE"
}

# GC pauses
# The template is:  $DATETIME: TIME: [GC pause, ($ACTION) ($COLLECTOR) ($CAUSE), $DURATION secs]
GCPAUSE="%s: 6686.150: [GC pause (%s) (%s), 1.1669987 secs]\n"
GCPAUSE_LONG="%s: 6686.150: [GC pause (%s) (%s) (%s), 1.1669987 secs]\n"

GC_ACTIONS=("G1 Evacuation Pause" "GCLocker Initiated GC" "G1 Humongous Allocation")
GC_COLLECTORS=("young" "mixed")
GC_CAUSES=("to-space exhausted" "initial-mark")

writeGCPause() {
    # $1 - action
    # $2 - collector
    printf "$GCPAUSE" $(date +%Y-%m-%dT%H:%M:%S.666%z) "$1" "$2" >> "$OUTFILE"
}

writeGCPauseLong() {
    # $1 - action
    # $2 - collector
    # $3 - cause
    printf "$GCPAUSE_LONG" $(date +%Y-%m-%dT%H:%M:%S.666%z) "$1" "$2" "$3" >> "$OUTFILE"
}

# GC events
# The template is: $DATETIME: TIME: [GC $EVENT SOMEDATA, $DURATION secs]
# *TIME* and *SOMEDATA* are unnecessary values
GCEVENT="%s: 16.834: [GC %s, 0.0250209 secs]\n"

GC_EVENTS=("concurrent-root-region-scan-end" "concurrent-mark-end" "concurrent-cleanup-end" "cleanup" "remark" "went-south")

writeGCEvent() {
    # $1 - event
    printf "$GCEVENT" $(date +%Y-%m-%dT%H:%M:%S.666%z) "$1" >> "$OUTFILE"
}

################################################################################
# main
################################################################################
while true; do
    writeFullGC $(((RANDOM % 50)+1)) $(((RANDOM % 6)+1))
    writeGCPause "${GC_ACTIONS[$RANDOM % ${#GC_ACTIONS[@]}]}" "${GC_COLLECTORS[$RANDOM % ${#GC_COLLECTORS[@]}]}"
    writeGCPauseLong "${GC_ACTIONS[$RANDOM % ${#GC_ACTIONS[@]}]}" "${GC_COLLECTORS[$RANDOM % ${#GC_COLLECTORS[@]}]}" "${GC_CAUSES[$RANDOM % ${#GC_CAUSES[@]}]}"
    writeGCEvent "${GC_EVENTS[$RANDOM % ${#GC_EVENTS[@]}]}"
    sleep 1
done
