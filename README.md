# Test Task

## Assumptions and limitations

As it's not a proper production code, I assume the following:

* Log entries written before analyzer start may be ignored
* The only log format supported is the one from the samples in letter
* Timestamps in logs are correct and can be used for metric reporting
* Log entries in log file may not be late for more then 5 minutes
* Scripts have all required rights and permissions
* All required dependencies are installed on the system
* Log entries considered single-lined, even though they are part of multi-line
  entry
* Log entries don't need to be checked for sanity
